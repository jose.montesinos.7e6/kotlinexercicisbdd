import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement

fun main(args: Array<String>) {
    /*val dataBaseFile = "BDExempleI.db"
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        val query = "SELECT * FROM usuari ORDER BY usu_nivell"
        val resultat: ResultSet = listStatement.executeQuery(query)

        while (resultat.next()) {
            val id = resultat.getInt("usu_id")
            val nom = resultat.getString("usu_nom")
            val nivell = resultat.getString("usu_nivell")
            println("$id - $nom - $nivell")
        }
    }//tancament de la connexió*/

    menu()
}

fun menu(){
    println("*******Menú principal*******\n" +
            "Selecciona quina acció vols fer:\n"+
            " - 1.Mostrar Usuaris\n" +
            " - 2.Inserir Usuari\n" +
            " - 3.Modificar Usuari\n" +
            " - 4.Eliminar Usuari\n" +
            " - 5.Sortir")
    var opc = readln()
    when(opc){
        "1" -> mostrarUsuaris()
        "2" -> InserirUsuari()
        "3" -> ModificarUsuari()
        "4" -> EliminarUsuari()
        "5" -> Sortir()
        else -> println("Error, has de premer un número de 1 al 5")
    }
}

fun mostrarUsuaris(){
    val dataBaseFile = "BDExempleI.db"
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        val query = "SELECT * FROM usuari ORDER BY usu_nivell, usu_nom"
        val resultat: ResultSet = listStatement.executeQuery(query)

        while (resultat.next()) {
            val id = resultat.getInt("usu_id")
            val nom = resultat.getString("usu_nom")
            val nivell = resultat.getString("usu_nivell")
            println("$id - $nom - $nivell")
        }
    }//tancament de la connexió
    println("Prem 0 per tornar al menú principal")
    var tornar = readln()
    while(tornar!="0"){
        println("Error, has de prèmer 0 per tornar al menú principal")
        tornar = readln()
    }
    menu()
}

fun InserirUsuari() {
    println("Introduceix el nom de l'usuari que vols introduïr")
    val nom = readln()
    println("A continuació introdueix el cognom")
    val cognom = readln()
    val nomCognom = "$nom.$cognom"
    println("Introdueix el nivell d'acces al que pertany l'usuari que vols introduïr, només pot ser docent, coordinador, directiu o administratiu")
    var nivell = readln().lowercase()
    while (nivell != "docent" && nivell != "cordinador" && nivell != "directiu" && nivell != "administratiu") {
        println("Error, l'usuari a introduïr només pot ser docent, coordinador, directiu o administratiu")
        nivell = readln().lowercase()
    }
    val dataBaseFile = "BDExempleI.db"
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        val query = "INSERT INTO usuari(usu_nom, usu_nivell) VALUES ('${nomCognom}', '${nivell}')"
        val statement = connection.prepareStatement(query)
        statement.execute()
    }
    println("Prém 0 per tornar al menú principal")
    var menu = readln()
    while(menu!="0"){
        println("Error! Has de premer el botó 0 per tornar")
        menu = readln()
    }
}

fun ModificarUsuari(){
    println("Introdueix l'Id de l'usuari al que vols modificar les seves dades")
    val idModificar = readln().toInt()

    if(comprovarExistencia(idModificar)){
        println("Escriu el nom del nou usuari")
        var nom = readln()
        println("Escriu el cognom del nou usuari")
        var cognom = readln()
        println("Escriu el nivell del usuari")
        var nivell = readln()

        var NomCognom = "$nom.$cognom"
        val dataBaseFile = "BDExempleI.db"
        DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
            val listStatement: Statement = connection.createStatement()
            val query = "UPDATE usuari set usu_nom='$NomCognom', usu_nivell='$nivell' where usu_id=$idModificar"
            val statement = connection.prepareStatement(query)
            statement.execute()
            println("Modificat")
        }
    }
    println("Prem 0 per tornar al menu principal")
    var tornar = readln()
    while(tornar!="0"){
        println("Error! Prem 0 per tornar al menu principal")
        tornar = readln()
    }
    menu()
}

fun EliminarUsuari(){
    println("Introdueix el Id de l'usuari que vols eliminar")
    var idEliminar = readln().toInt()

    if(comprovarExistencia(idEliminar)) {
        val dataBaseFile = "BDExempleI.db"
        DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
            val listStatement: Statement = connection.createStatement()
            val query = "DELETE FROM usuari WHERE usu_id = $idEliminar"
            val statement = connection.prepareStatement(query)
            statement.execute()
            println("Eliminat")
        }
    }
    println("Prem 0 per tornal al menu principal")
    var tornar = readln()
    while(tornar!="0"){
        println("Error! Prem 0 per tornar al menu principal")
        tornar = readln()
    }
    menu()
}

fun comprovarExistencia(id:Int):Boolean{

    val dataBaseFile = "BDExempleI.db"
    DriverManager.getConnection("jdbc:sqlite:$dataBaseFile").use { connection ->
        val listStatement: Statement = connection.createStatement()
        val query = "SELECT * FROM usuari where usu_id=$id"
        val resultat: ResultSet = listStatement.executeQuery(query)

        if(resultat.next()){
            println("Usari actual:")
            val id = resultat.getInt("usu_id")
            val nom = resultat.getString("usu_nom")
            val nivell = resultat.getString("usu_nivell")
            println("$id - $nom - $nivell")
            return true
            /*
            val id = resultat.getInt("usu_id")
            val nom = resultat.getString("usu_nom")
            val nivell = resultat.getString("usu_nivell")
            println("$id - $nom - $nivell")*/
        }else{
            println("Error! No existeix l'usuari")
            return false
        }
    }
}

fun Sortir(){
    println("Fins un altre!")
}